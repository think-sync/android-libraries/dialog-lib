package com.thinknsync.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;


public class BooleanDialog extends BaseDialog<Boolean> {

    private TextView titleTextView, messageTextView;
    private Button positiveButton, negativeButton;

    public BooleanDialog(Context context) {
        super(context);
        positiveText = context.getResources().getString(R.string.boolean_yes_label);
        negativeText = context.getResources().getString(R.string.boolean_no_label);
        buildDialog();
    }

    @Override
    public void buildDialog() {
        dialog = new Dialog(context, R.style.DefaultDialog);
        dialog.setContentView(R.layout.boolean_dialog);
        initView();
        initValues();
        initViewListeners();
    }

    @Override
    public void setTitle(String title) {
        super.setTitle(title);
        titleTextView.setText(title);
        if(title != null && !title.equals("")) {
            titleTextView.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void setMessage(String message) {
        super.setMessage(message);
        messageTextView.setText(message);
    }

    private void initViewListeners() {
        positiveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideDialog();
                dialogResponse.onPositiveClicked(true);
            }
        });

        negativeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideDialog();
                dialogResponse.onNegativeClicked(false);
            }
        });
    }

    private void initView() {
        titleTextView = dialog.findViewById(R.id.dialog_title);
        messageTextView = dialog.findViewById(R.id.dialog_message);
        positiveButton = dialog.findViewById(R.id.yes_button);
        negativeButton = dialog.findViewById(R.id.no_button);
    }

    private void initValues() {
        titleTextView.setText(title);
        messageTextView.setText(message);
        positiveButton.setText(positiveText);
        negativeButton.setText(negativeText);
    }
}
