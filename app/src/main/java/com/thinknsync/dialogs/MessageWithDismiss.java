package com.thinknsync.dialogs;

import android.content.Context;

import androidx.appcompat.app.AlertDialog;

public class MessageWithDismiss extends BaseDialog<Void> {

    public MessageWithDismiss(Context context){
        super(context);
    }

    @Override
    public void buildDialog(){
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(title)
                .setMessage(message)
                .setCancelable(false)
                .setNegativeButton(positiveText, null);
        dialog = builder.create();
    }
}
