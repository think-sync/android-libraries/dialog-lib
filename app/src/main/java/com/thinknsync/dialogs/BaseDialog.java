package com.thinknsync.dialogs;

import android.app.Dialog;
import android.content.Context;

public abstract class BaseDialog<T> implements DialogInterface<T> {

    protected Context context;
    protected Dialog dialog;
    protected String title = "", message = "", positiveText = "Yes", negativeText = "No";
    protected boolean isCancellable = true;
    protected T infoObject;
    protected DialogResponse<T> dialogResponse;

    public BaseDialog(Context context){
        this.context = context;
        buildDialog();
    }

    @Override
    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public void setPositiveText(String positiveText) {
        this.positiveText = positiveText;
    }

    @Override
    public void setNegativeText(String negativeText) {
        this.negativeText = negativeText;
    }

    @Override
    public void setCancellable(boolean cancellable) {
        isCancellable = cancellable;
    }

    @Override
    public void showDialog(){
        if(!dialog.isShowing()){
            dialog.show();
        }
    }

    @Override
    public void hideDialog(){
        if(dialog.isShowing()){
            dialog.dismiss();
        }
    }

    @Override
    public void setInformationObject(T infoObject) {
        this.infoObject = infoObject;
    }

    @Override
    public void setDialogResponse(DialogResponse response) {
        this.dialogResponse = response;
    }
}
