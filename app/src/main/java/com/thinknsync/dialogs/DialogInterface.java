package com.thinknsync.dialogs;

public interface DialogInterface<T> {
    void setTitle(String title);
    void setMessage(String message);
    void setPositiveText(String positiveText);
    void setNegativeText(String negativeText);
    void setCancellable(boolean cancellable);
    void buildDialog();
    void showDialog();
    void hideDialog();
    void setInformationObject(T infoObject);
    void setDialogResponse(DialogResponse<T> response);

    interface DialogResponse<T> {
        void onPositiveClicked(T infoObject);
        void onNegativeClicked(T infoObject);
    }
}
